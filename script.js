const signs = ["rock", "paper", "scissors"];
const scores = [0, 0]; // playerScore, computerScore
const displayScores = document.querySelectorAll(".score");
const history = document.querySelector("#history");
const result = document.querySelector("#result");
const ROUNDS_TO_WIN = 5;

function computerPlay() {
    return signs[Math.floor(Math.random()*3)];
}

function playRound(playerSelection, computerSelection) {
    playerSelection = playerSelection.toLowerCase();
    let winner;
    const fail = `You lose, ${computerSelection} beats ${playerSelection}`;
    const win = `You win, ${playerSelection} beats ${computerSelection}`;


    if (playerSelection === computerSelection) {
        winner = "equality";
    } else if (playerSelection === "rock") {
        if (computerSelection === "paper") {
            winner = "computer";
        } else if (computerSelection === "scissors") {
            winner = "player";
        }
    } else if (playerSelection === "paper") {
        if (computerSelection === "scissors") {
            winner = "computer";
        } else if (computerSelection === "rock") {
            winner = "player";
        }
    } else if (playerSelection === "scissors") {
        if (computerSelection === "rock") {
            winner = "computer";
        } else if (computerSelection === "paper") {
            winner = "player";
        }
    }

    const roundRecap = document.createElement("p"); // feed the history
    switch (winner) {
        case "player":
            roundRecap.innerText = `You win, ${playerSelection} beats ${computerSelection}`;
            scores[0]++; // On met les scores à jour, ils s'afficheront en dehors de la fonction
            break;
        case "computer":
            roundRecap.innerText = `You lose, ${computerSelection} beats ${playerSelection}`;
            scores[1]++
            break;
        case "equality":
            roundRecap.innerText = `Equality of ${playerSelection}`;
            break;
        default:
            console.error("problem in playRound");
            break;
    }
    history.insertBefore(roundRecap, history.firstChild);

    return winner;
}


function game(numberOfRounds) {
    scores = [0, 0];
    for (let i = 0; i < numberOfRounds; i++) {
        let roundResult = playRound(prompt("Rock, paper or scissors ?"), computerPlay());
        console.log(roundResult);
    }
    return;
}

function buttonPressed(ev) {
    console.log(this.classList[1])
    playRound(this.classList[1], computerPlay());
    for (i in [...displayScores]) { // updates the displayed scores
        displayScores[i].innerText = scores[i];
    }
    if (scores[0] === ROUNDS_TO_WIN || scores[1] === ROUNDS_TO_WIN) {
        choices.forEach(button => button.removeEventListener("click", buttonPressed));
        result.append(`${(scores[0] > scores[1]) ? "Victory !" : "defeat..."}`);
        choices.forEach(btn => btn.remove());
    }
}

const choices = document.querySelectorAll(".choice");
choices.forEach(button => button.addEventListener("click", buttonPressed));


// game(5); // Would be better to launch it by event (and send victory and defeit as callbacks ?)
/*
if(playerScore > computerScore) {
    console.log("victory");
} else if(playerScore < computerScore) {
    console.log("The computer won");
} else console.log("Equality");
*/
